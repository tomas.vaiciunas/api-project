package main

import (
	"fmt"
	"net/http"

	"core-platform/api-project/config"
	"core-platform/api-project/handlers"
	"core-platform/api-project/migrations"
)

func main() {
	fmt.Println("Server started")
	config.DbConfiguration()
	migrations.IndexMigration()

	server := &http.Server{
		Addr:    ":8080",
		Handler: handlers.IndexRouting(),
	}
	server.ListenAndServe()

}
