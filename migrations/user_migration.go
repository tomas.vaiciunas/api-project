package migrations

import (
	"core-platform/api-project/config"
	"core-platform/api-project/models"
)

func UserMigration() {
	config.DB.AutoMigrate(&models.User{})
}
