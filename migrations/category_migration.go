package migrations

import (
	"core-platform/api-project/config"
	"core-platform/api-project/models"
)

func CategoryMigration() {
	config.DB.AutoMigrate(&models.Category{})
}
