package migrations

import (
	"core-platform/api-project/config"
	"core-platform/api-project/models"
)

func ProductMigration() {
	config.DB.AutoMigrate(&models.Product{})
}
